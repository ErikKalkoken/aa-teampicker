# Team Picker

This app can distribute users randomly and secretly to a set number of teams.

[![release](https://img.shields.io/pypi/v/aa-teampicker?label=release)](https://pypi.org/project/aa-teampicker/)
[![python](https://img.shields.io/pypi/pyversions/aa-teampicker)](https://pypi.org/project/aa-teampicker/)
[![django](https://img.shields.io/pypi/djversions/aa-teampicker?label=django)](https://pypi.org/project/aa-teampicker/)
[![pipeline](https://gitlab.com/ErikKalkoken/aa-teampicker/badges/master/pipeline.svg)](https://gitlab.com/ErikKalkoken/aa-teampicker/-/pipelines)
[![coverage report](https://gitlab.com/ErikKalkoken/aa-teampicker/badges/master/coverage.svg)](https://gitlab.com/ErikKalkoken/aa-teampicker/-/commits/master)
[![license](https://img.shields.io/badge/license-MIT-green)](https://gitlab.com/ErikKalkoken/aa-teampicker/-/blob/master/LICENSE)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![chat](https://img.shields.io/discord/790364535294132234)](https://discord.gg/zmh52wnfvM)

>**Important**:<br>This app is in development and not yet ready for production use.
