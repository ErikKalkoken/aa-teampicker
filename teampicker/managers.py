from django.db import models
from django.db.models import Count


class CrowdQuerySet(models.QuerySet):
    def annotate_num_members(self):
        return self.annotate(num_members=Count("teams__members__pk"))


class CrowdManagerBase(models.Manager):
    pass


CrowdManager = CrowdManagerBase.from_queryset(CrowdQuerySet)
