from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy
from django.views.generic import DeleteView, DetailView, ListView
from django.views.generic.edit import CreateView

from teampicker.models import Crowd, TeamMember


@login_required
def index_view(request):
    return redirect("teampicker:crowd-list")


@login_required
@permission_required("teampicker.basic_access")
def team_member_delete_view(request, pk: int):
    member = get_object_or_404(TeamMember, pk=pk)
    team = member.team
    crowd = team.crowd
    member.delete()
    messages.info(
        request,
        f"You have have left team {team.name} of the {crowd.name} crowd with one member.",
    )
    return redirect("teampicker:crowd-join", crowd.pk)


@login_required
@permission_required("teampicker.basic_access")
def crowd_join_view(request, pk: str):
    crowd = get_object_or_404(Crowd, pk=pk)
    try:
        crowd.join_random_team(request.user)
    except OverflowError:
        messages.warning(
            request,
            "Failed to join. You have already reached the maximum number of memberships.",
        )
    return redirect("teampicker:crowd-join", crowd.pk)


@login_required
@permission_required("teampicker.basic_access")
def crowd_clear_view(request, pk: str):
    crowd = get_object_or_404(Crowd, pk=pk)
    TeamMember.objects.filter(team__crowd=crowd).delete()
    return redirect("teampicker:crowd-detail", crowd.pk)


class CrowdListView(PermissionRequiredMixin, LoginRequiredMixin, ListView):
    model = Crowd
    permission_required = "teampicker.basic_access"

    def get_queryset(self):
        qs = super().get_queryset()
        return (
            qs.filter(
                teams__members__user=self.request.user,
                created_at__gt=Crowd.stale_cutoff(),
            )
            .order_by("-created_at")
            .distinct()
        )


class CrowdJoinView(PermissionRequiredMixin, LoginRequiredMixin, DetailView):
    model = Crowd
    permission_required = "teampicker.basic_access"
    template_name_suffix = "_join"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        team_members = (
            TeamMember.objects.filter(team__crowd=self.object, user=self.request.user)
            .select_related("team")
            .order_by("team__num")
        )
        context["team_members"] = team_members
        context["num_team_members"] = team_members.count()
        return context


class CrowdManageView(PermissionRequiredMixin, LoginRequiredMixin, ListView):
    model = Crowd
    template_name_suffix = "_manage"
    permission_required = "teampicker.manage_crowds"

    def get_queryset(self):
        qs = super().get_queryset()
        return (
            qs.filter(created_by=self.request.user, created_at__gt=Crowd.stale_cutoff())
            .annotate_num_members()
            .order_by("-created_at")
        )


class CrowdCreateView(PermissionRequiredMixin, LoginRequiredMixin, CreateView):
    model = Crowd
    fields = ["name", "description", "num_teams", "max_members"]
    permission_required = "teampicker.manage_crowds"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)


class CrowdDeleteView(PermissionRequiredMixin, LoginRequiredMixin, DeleteView):
    model = Crowd
    permission_required = "teampicker.manage_crowds"
    success_url = reverse_lazy("teampicker:crowd-manage")


class CrowdDetailView(PermissionRequiredMixin, LoginRequiredMixin, DetailView):
    model = Crowd
    permission_required = "teampicker.manage_crowds"

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.annotate_num_members()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["teams"] = self.object.teams.order_by("num")
        return context
