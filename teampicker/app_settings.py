from app_utils.django import clean_setting

TEAMPICKER_CROWD_STALE_HOURS = clean_setting("TEAMPICKER_CROWD_STALE_HOURS", 24)
